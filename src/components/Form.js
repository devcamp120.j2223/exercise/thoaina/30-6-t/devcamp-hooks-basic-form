import { useState, useEffect } from "react";

function Form() {
    const [firstname, setFirstname] = useState(localStorage.getItem("firstname"));
    const [lastname, setLastname] = useState(localStorage.getItem("lastname"));

    const changeFirstnameHandler = (event) => {
        setFirstname(event.target.value);
        console.log(event.target.value);
    }

    const changeLastnameHandler = (event) => {
        setLastname(event.target.value);
        console.log(event.target.value);
    }

    useEffect(() => {
        localStorage.setItem("firstname", firstname);
        localStorage.setItem("lastname", lastname);
    })

    return (
        <div>
            <div>
                <input value={firstname} onChange={changeFirstnameHandler} placeholder="Firstname" />
            </div>
            <div>
                <input value={lastname} onChange={changeLastnameHandler} placeholder="Lastname" />
            </div>
            <div>
                <p>{firstname} {lastname}</p>
            </div>
        </div>
    )
}

export default Form;